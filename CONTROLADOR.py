from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import *
from numba import jit, cuda
from PIL import ImageTk, Image
import random
import numpy as np
#import pandas as pd
import tkinter as tk
import matplotlib
from timeit import default_timer as timer

matplotlib.use('TkAgg')

cuad = []
creditos = 0


@jit(nopython=True)
def genCuad(array, cuad):
    mq = [0.1111, 0.2222, 0.3333, 0.4444, 0.5555, 0.6666, 0.7777, 0.8888, 1.0]
    x = np.random.uniform(0, 1, 3)
    cuad = []
    for k in range(len(x)):
        if x[k] <= mq[0]:  # siete
            cuad.append(array[0])
        elif x[k] <= mq[1]:  # bar
            cuad.append(array[1])
        elif x[k] <= mq[2]:  # campana
            cuad.append(array[2])
        elif x[k] <= mq[3]:  # sandia
            cuad.append(array[3])
        elif x[k] <= mq[4]:  # uva
            cuad.append(array[4])
        elif x[k] <= mq[5]:  # naranja
            cuad.append(array[5])
        elif x[k] <= mq[6]:  # cereza
            cuad.append(array[6])
        elif x[k] <= mq[7]:  # banana
            cuad.append(array[7])
        elif x[k] <= mq[8]:  # banana
            cuad.append(array[8])

    for i in range(len(x)):
        if x[i] <= mq[0]:  # siete
            cuad.append(array[0])
            mq[1] = mq[1] - 0.0755
        elif x[i] <= mq[1]:  # bar
            cuad.append(array[1])
            mq[0] = mq[0] + 0.0755
        elif x[i] <= mq[2]:  # campana
            cuad.append(array[2])
            mq[1] = mq[1] + 0.0755
        elif x[i] <= mq[3]:  # sandia
            cuad.append(array[3])
            mq[2] = mq[2] + 0.0755
        elif x[i] <= mq[4]:  # uva
            cuad.append(array[4])
            mq[3] = mq[3] + 0.0755
        elif x[i] <= mq[5]:  # naranja
            cuad.append(array[5])
            mq[4] = mq[4] + 0.0755
        elif x[i] <= mq[6]:  # cereza
            cuad.append(array[6])
            mq[5] = mq[5] + 0.0755
        elif x[i] <= mq[7]:  # banana
            cuad.append(array[7])
            mq[6] = mq[6] + 0.0755
        elif x[i] <= mq[8]:  # limon
            cuad.append(array[8])
            mq[7] = mq[7] + 0.0755

    return cuad

def cuadCasilleros(array):
    mq = [0.1111, 0.2222, 0.3333, 0.4444, 0.5555, 0.6666, 0.7777, 0.8888, 1.0]
    x = np.random.uniform(0, 1, 3)
    cuad = []
    for k in range(len(x)):
        if x[k] <= mq[0]:  # siete
            cuad.append(array[0])
        elif x[k] <= mq[1]:  # bar
            cuad.append(array[1])
        elif x[k] <= mq[2]:  # campana
            cuad.append(array[2])
        elif x[k] <= mq[3]:  # sandia
            cuad.append(array[3])
        elif x[k] <= mq[4]:  # uva
            cuad.append(array[4])
        elif x[k] <= mq[5]:  # naranja
            cuad.append(array[5])
        elif x[k] <= mq[6]:  # cereza
            cuad.append(array[6])
        elif x[k] <= mq[7]:  # banana
            cuad.append(array[7])
        elif x[k] <= mq[8]:  # banana
            cuad.append(array[8])
    return cuad


def gen_cuad(caja, imagenes, cas1, cas2, cas3):  # no es de mi interes att paul
    global creditos
    if (creditos <= 0):
        caja.config(text='0')
        print("NO TIENE MAS CREDITOS")
    else:
        creditos = creditos - 1
        caja.config(text=creditos)
        cuad = []
        x = np.random.uniform(0, 1, 3)  # [x1,x2,x3]
        cuad = cuadCasilleros(imagenes)
        for k in range(len(x)):
            if k == 0:
                cas1.config(image=cuad[k])
            elif k == 1:
                cas2.config(image=cuad[k])
            else:
                cas3.config(image=cuad[k])
        wtri = 0
        wdob = 0
        loss = 0
        
        if cuad[0] == cuad[1] and cuad[1] == cuad[2]:
            wtri = wtri + 1
            creditos = creditos + 5
            caja.config(text=creditos)
        elif cuad[0] == cuad[1] or cuad[0] == cuad[2] or cuad[1] == cuad[2]:
            wdob = wdob + 1
            creditos = creditos + 3
            caja.config(text=creditos)
        else:
            loss = loss + 1


def createNewWindow(raiz, cajita):
    newWindow = tk.Toplevel(raiz)
    newWindow.config(bg='#0593B9')
    newWindow.config(width='300', height='200')
    newWindow.config(bd=10)
    newWindow.config(relief='groove')

    lblnewWindow = Label(newWindow, text='Cuantos creditos desea agregar?')
    lblnewWindow.config(bg='#0593B9', font=('Arial', 10), fg='White')
    lblnewWindow.place(x=1, y=20)

    # txt de agregar creditos creditos
    txtnewWindow = Text(newWindow)
    txtnewWindow.config(width='10', height='3', bd=5, relief='groove')
    txtnewWindow.place(x=75, y=60)
    # Boton de agregar
    btnagregarc = Button(newWindow, text='Agregar', command=lambda: recuperarCreditos(
        txtnewWindow, newWindow, cajita))
    btnagregarc.config(bg='#bf930d', width='7', height='1',
                       fg='White', bd=10, relief='groove')
    btnagregarc.place(x=70, y=120)


def recuperarCreditos(x, ventana, caja):
    global creditos
    inputValue = x.get("1.0", 'end-1c')
    print(inputValue)
    crd = int(inputValue)
    if (crd < 0):
        print("El numero es menor a 0")
    else:
        creditos = crd + creditos
        caja.config(text=creditos)
        cerrar(ventana)
        return creditos


def cerrar(x):
    x.destroy()


def ingresarJugadas(raiz):
    newWindow = tk.Toplevel(raiz)
    newWindow.config(bg='#0593B9')
    newWindow.config(width='300', height='200')
    newWindow.config(bd=10)
    newWindow.config(relief='groove')

    lblnewWindow = Label(newWindow, text='Cuantas jugadas desea probar?')
    lblnewWindow.config(bg='#0593B9', font=('Arial', 10), fg='White')
    lblnewWindow.place(x=1, y=20)

    # txt de agregar numero de jugadas
    txtnewWindow = Text(newWindow)
    txtnewWindow.config(width='10', height='1.5', bd=5, relief='groove')
    txtnewWindow.place(x=75, y=60)

    # Boton de agregar
    btnagregarc = Button(newWindow, text='Agregar', command=lambda: recuperarJugadas(
        txtnewWindow, newWindow, raiz))
    btnagregarc.config(bg='#bf930d', width='7', height='1',
                       fg='White', bd=10, relief='groove')
    btnagregarc.place(x=70, y=120)


def recuperarJugadas(x, ventana, raiz):
    inputValue = x.get("1.0", 'end-1c')
    print(inputValue)
    num = int(inputValue)
    if (num <= 0):
        print("El numero es 0")
    else:
        cerrar(ventana)
        gen_game(num, raiz)


def gen_game(n, raiz):
    start = timer()
    resuni = calc_prob(n)
    # print("Con CPU:", timer() - start)
    print("Con GPU:", timer() - start)
    unidades = [resuni[0],resuni[1],resuni[2]]
    unidadesnu = [resuni[3],resuni[4],resuni[5]]

    textote = ('DISTRIBUCION UNIFORME')
    textote = textote + (' \n3 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidades[0],round((unidades[0]/n*100),2)))
    textote = textote + (' \n2 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidades[1],round((unidades[1]/n*100),2)))
    textote = textote + (' \n0 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidades[2],round((unidades[2]/n*100),2)))
    textotenu = ('DISTRIBUCION NO UNIFORME')
    textotenu = textotenu + (' \n3 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidadesnu[0],round((unidadesnu[0]/n*100),2)))
    textotenu = textotenu + (' \n2 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidadesnu[1],round((unidadesnu[1]/n*100),2)))
    textotenu = textotenu + (' \n0 Casilleros Iguales -> {} veces - prob -> {}%'.format(unidadesnu[2],round((unidadesnu[2]/n*100),2)))

    nuevaVentana2 = tk.Toplevel(raiz)
    nuevaVentana2.title('Estadisticas')
    # Frame
    estadistica = Frame(nuevaVentana2)
    estadistica.pack()
    estadistica.config(bg='#0593B9')
    estadistica.config(width='1000', height='630')
    estadistica.config(bd=10)
    estadistica.config(relief='groove')

    # Laber de probabilidades
    lblcreditos = Label(estadistica, text='PROBABILIDADES')
    lblcreditos.config(bg='#0593B9', font=('Arial', 17), fg='White')
    lblcreditos.place(x=300, y=10)

    # txt de numero de jugadas
    txtjugadas = Label(estadistica, text=n)
    txtjugadas.config(width='8', height='2', bd=5, relief='groove')
    txtjugadas.place(x=550, y=5)

    # Laber de jugadas
    lbljugadas = Label(estadistica, text='Jugadas')
    lbljugadas.config(bg='#0593B9', font=('Arial', 15), fg='White')
    lbljugadas.place(x=630, y=10)

    # Label con probabilidades de Distribucion uniforme
    lbljugadas = Label(estadistica, text=textote, font=("Verdana", 8))
    lbljugadas.config(width='45', height='5', bg='white')
    lbljugadas.place(x=150, y=50)

    # Label PLOT
    lbljugadas = Label(estadistica)
    lbljugadas.config(width='80', height='20')
    lbljugadas.place(x=150, y=150)

    # Label con probabilidades de Distribucion no Uniforme
    lbljugadasNU = Label(estadistica, text=textotenu, font=("Verdana", 8))
    lbljugadasNU.config(width='45', height='5', bg='white')
    lbljugadasNU.place(x=500, y=50)

    # Boton de volver
    btnvolver = Button(estadistica, text='Volver',
                       command=lambda: cerrar(nuevaVentana2))
    btnvolver.config(bg='#bf930d', width='10', height='1',
                     fg='White', bd=15, relief='groove')
    btnvolver.place(x=20, y=530)

    plot(unidades, unidadesnu, lbljugadas)
    # plot(,lbljugadasNU)


@jit(nopython=True)
def calc_prob(n):
    arr = ['siete', 'bar', 'campana', 'sandia',
             'uva', 'naranja', 'cereza', 'banana', 'limon']
    cuad = ['','']
    wtri, wtrinu = 0, 0
    wdob, wdobnu = 0, 0
    loss, lossnu = 0, 0
    for i in range(n):
        cuad = genCuad(arr, cuad)
        if cuad[0] == cuad[1] and cuad[1] == cuad[2]:
            wtri = wtri + 1
        elif cuad[0] == cuad[1] or cuad[0] == cuad[2] or cuad[1] == cuad[2]:
            wdob = wdob + 1
        else:
            loss = loss + 1

        if cuad[3] == cuad[4] and cuad[4] == cuad[5]:
            wtrinu = wtrinu + 1
        elif cuad[3] == cuad[4] or cuad[3] == cuad[5] or cuad[4] == cuad[5]:
            wdobnu = wdobnu + 1
        else:
            lossnu = lossnu + 1

        cuad = ['','']

    unidades = (wtri,wdob,loss,wtrinu,wdobnu,lossnu)
          
    return unidades


def plot(unidades, unidadesnu, label):
    cas = ['3 Casilleros Iguales', '2 Casilleros Iguales', '0 Casilleros Iguales']
    # men_means = [20, 34, 30, 35, 27]
    # women_means = [25, 32, 34, 20, 25]

    # Obtenemos la posicion de cada etiqueta en el eje de X
    x = np.arange(len(cas))
    # tamaño de cada barra
    width = 0.35

    fig = Figure(figsize=(7.1, 4.4))  # ancho, largo
    ax = fig.add_subplot(111)

    # Generamos las barras para el conjunto de hombres
    rects1 = ax.bar(x - width / 2, unidades, width, label='Dist. Uniforme')
    # Generamos las barras para el conjunto de mujeres
    rects2 = ax.bar(x + width / 2, unidadesnu,
                    width, label='Dist. No Uniforme')

    # Añadimos las etiquetas de identificacion de valores en el grafico
    ax.set_ylabel('Porcentaje')
    ax.set_title('Porcentaje Dist. Uniforme y no Uniforme')
    ax.set_xticks(x)
    ax.set_xticklabels(cas)
    # Añadimos un legen() esto permite mmostrar con colores a que pertence cada valor.
    ax.legend()
    autolabel(rects1, ax)
    autolabel(rects2, ax)
    # fig.tight_layout()
    # plt.savefig('doble_barra.png')

    canvas = FigureCanvasTkAgg(fig, master=label)
    canvas.get_tk_widget().pack()
    canvas.draw()


def autolabel(rects, ax):
    """Funcion para agregar una etiqueta con el valor en cada barra"""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

