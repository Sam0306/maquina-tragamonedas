from tkinter import *
from PIL import ImageTk, Image
import CONTROLADOR as cont
import random
import numpy as np
import tkinter as tk

#Direcciones de imagenes
siente = 'D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/siete.png'
bar = 'D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/bar.png'
sandia = 'D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/sandia.png'
campana = 'D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/campana.png'
uva = 'D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/uva.png'

#Ventana Raiz
raiz= tk.Tk()
raiz.title('Maquina Tragamonedas')

#IMAGENES GRANDES REDIMENSIONADAS
uvax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/uvaX.png")
uva1 = uvax.resize((110,110), Image.ANTIALIAS)
newUva= ImageTk.PhotoImage(uva1)

barx = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/barX.png")
bar1 = barx.resize((110,110), Image.ANTIALIAS)
newBar= ImageTk.PhotoImage(bar1)

campanax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/campanaX.png")
campana1 = campanax.resize((110,110), Image.ANTIALIAS)
newCampana= ImageTk.PhotoImage(campana1)

sandiax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/sandiaX.png")
sandia1 = sandiax.resize((110,110), Image.ANTIALIAS)
newSandia= ImageTk.PhotoImage(sandia1)

sietex = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/sieteX.png")
siete1 = sietex.resize((110,110), Image.ANTIALIAS)
newSiete= ImageTk.PhotoImage(siete1)

naranjax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/naranja.png")
naranja1 = naranjax.resize((110,110), Image.ANTIALIAS)
newNaranja= ImageTk.PhotoImage(naranja1)

cerezax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/cereza.png")
cereza1 = cerezax.resize((110,110), Image.ANTIALIAS)
newCereza= ImageTk.PhotoImage(cereza1)

bananax = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/bananaX.png")
banana1 = bananax.resize((110,110), Image.ANTIALIAS)
newBanana= ImageTk.PhotoImage(banana1)

limonx = Image.open("D:/Users/VEL-USER/Documents/Pruebitas Python/Proyecto Final/Imagenes/limon.png")
limon1 = limonx.resize((110,110), Image.ANTIALIAS)
newLimon= ImageTk.PhotoImage(limon1)


#Frame
ventana=Frame()
ventana.pack()
ventana.config(bg='#0593B9')
ventana.config(width='500', height='570')
ventana.config(bd=10)
ventana.config(relief='groove')

#*************************Parte de Premios******************************

#Laberl de Premios
lbltitulo= Label(ventana, text='PREMIOS')
lbltitulo.config(bg='#0593B9',font=('Arial',20),fg='White')
lbltitulo.place(x=180, y=10)

#Laberl de 90 creditos
lblrepe1= Label(ventana, text='Obtener 3 Figuras iguales ---> 5 Creditos')
lblrepe1.config(bg='#0593B9',font=('Arial',12), fg='White')
lblrepe1.place(x=160, y=55)

#Laberl de 70 creditos
lblrepe2= Label(ventana, text='Obtener 2 Figuras iguales ---> 3 Creditos')
lblrepe2.config(bg='#0593B9',font=('Arial',12), fg='White')
lblrepe2.place(x=160, y=95)

#Laberl de 40 creditos
lblrepe3= Label(ventana, text='Obtener 0 Figuras iguales ---> 0 Creditos')
lblrepe3.config(bg='#0593B9',font=('Arial',12), fg='White')
lblrepe3.place(x=160, y=135)


#3 Imagenes de 7
imgsiete= ImageTk.PhotoImage(Image.open(siente))
imsiete1= Label(ventana, image = imgsiete)
imsiete1.place(x=50, y=50)
imsiete2= Label(ventana, image = imgsiete)
imsiete2.place(x=85, y=50)
imsiete3= Label(ventana, image = imgsiete)
imsiete3.place(x=115, y=50)

#3 Imagenes de bar
imgbar= ImageTk.PhotoImage(Image.open(bar))
imbar1= Label(ventana, image = imgbar)
imbar1.place(x=50, y=90)
imbar2= Label(ventana, image = imgbar)
imbar2.place(x=85, y=90)
imsiete5= Label(ventana, image = imgsiete)
imsiete5.place(x=115, y=90)

#2 veces siete y 1 sandia
imgcampana= ImageTk.PhotoImage(Image.open(campana))
imgsandia= ImageTk.PhotoImage(Image.open(sandia))
imcampana1= Label(ventana, image = imgcampana)
imcampana1.place(x=50, y=130)
imsiete5= Label(ventana, image = imgsiete)
imsiete5.place(x=85, y=130)
imsandia1= Label(ventana, image = imgsandia)
imsandia1.place(x=115, y=130)

#*************************Parte de casilleros******************************

lblcasillero1 = Label(ventana, image = newLimon)
lblcasillero1.place(x=35, y= 200)

lblcasillero2 = Label(ventana, image = newNaranja)
lblcasillero2.place(x=175, y= 200)

lblcasillero3 = Label(ventana, image = newBanana)  
lblcasillero3.place(x=315, y= 200)

#Label de creditos disponibles
lblcreddispo= Label(ventana, text='Creditos disponibles')
lblcreddispo.config(bg='#0593B9',font=('Arial',12),fg='White')
lblcreddispo.place(x=10, y=430)

#txt de creditos disponibles
txtcreddispo= Label(ventana, text= '0')
txtcreddispo.config(width='10', height='3', bd=2, relief='groove', font = ('Arial',12))
txtcreddispo.place(x=35, y=460)

#ARREGLO DE IMAGENES  
array = [newSiete, newBar, newCampana,newSandia, newUva, newNaranja, newCereza, newBanana, newLimon]

#Boton Jugar
btnjugar = Button(ventana, text='JUGAR', command=lambda: cont.gen_cuad(txtcreddispo,array,lblcasillero1, lblcasillero2, lblcasillero3))
btnjugar.config(bg='#bf930d', width='10', height='1', font=('Arial',17), fg='White',bd=10, relief='groove' )
btnjugar.place(x=160, y=350)

#Boton Agregar creditos 
btnagregar = Button(ventana, text='Agregar creditos', command =lambda: cont.createNewWindow(raiz,txtcreddispo))
btnagregar.config(bg='#bf930d', width='15', height='1', font=('Arial',10), fg='White',bd=10, relief='groove' )
btnagregar.place(x=310, y=425)

#Boton Agregar creditos 
btnprob = Button(ventana, text='Probabilidades', command =lambda: cont.ingresarJugadas(raiz))
btnprob.config(bg='#bf930d', width='15', height='1', font=('Arial',10), fg='White',bd=10, relief='groove' )
btnprob.place(x=310, y=475)


raiz.mainloop()

